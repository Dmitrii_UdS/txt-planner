﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace txt_planner
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var fileStream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "plan.txt", FileMode.Open))
                {
                    string wholeFile = new StreamReader(fileStream, Encoding.UTF8).ReadToEnd();
                    string[] rows = wholeFile.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);


                    DateTime timeTaskEnds = DateTime.Now;
                    string result = "";
                    int minutesToAdd = 0;
                    for (int i = 0; i < rows.Length; ++i)
                    {
                        minutesToAdd = Convert.ToInt32(rows[i].Substring(0, rows[i].IndexOf(' ')));
                        timeTaskEnds = timeTaskEnds.AddMinutes(minutesToAdd);
                        result += String.Format("{0} {1} {2}\r\n", 
                            timeTaskEnds.Hour, 
                            timeTaskEnds.Minute, 
                            rows[i].Substring(rows[i].IndexOf(' ')+1));
                    }

                    using (var outputFileStream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "timings.txt", FileMode.Create))
                    {
                        var outputFile = new StreamWriter(outputFileStream, Encoding.UTF8);
                        outputFile.Write(result);
                        outputFile.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }
    }
}
